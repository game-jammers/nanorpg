//
// (c) GameJammers 2022
// http://www.jamming.games
//

//
// ############################################################################
//

class QuestObjective {
	constructor({requires, unlocks}) {
		this.requires = requires;
		this.unlocks = unlocks;
	}
}

//
// ############################################################################
//

class Quest {
	constructor() {
	}

	//
	// public methods ///////////////////////////////////////////////////////////
	//
	
	get chain() {
	}

} // end class

Quest.Stage = QuestStage;

//
// ############################################################################
//

export default Quest;
