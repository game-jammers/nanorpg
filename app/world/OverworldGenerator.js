//
// (c) GameJammers 2022
// http://www.jamming.games
//

import Generator from './Generator.js'

//
// ############################################################################
//

class OverworldGenerator extends Generator {
	constructor(seed) {
		super(seed);
	}

	//
	// public methods ///////////////////////////////////////////////////////////
	//
	
	build(assets) {
		console.log(assets.textures.overworld.ids);
	}
}

//
// ############################################################################
//

export default OverworldGenerator;
