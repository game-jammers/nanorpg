//
// (c) GameJammers 2022
// http://www.jamming.games
//

import Random from '@app/math/Random.js'

//
// ############################################################################
//

class Generator {
	constructor(seed) {
		this.rng = Random.rng(seed)
	}
} // end class

//
// ############################################################################
//

export default Generator;
