//
// (c) GameJammers 2022
// http://www.jamming.games
//

import * as PIXI from 'pixi.js';
import Signal from '@app/util/Signal.js';

//
// ############################################################################
//

class AssetManager {
	constructor(manifest) {
		this.manifest = manifest;
		this.onLoaded = new Signal('onLoaded');
		this.onProgress = new Signal('onProgress');
		this.textures = [];
	}

	//
	// public methods ///////////////////////////////////////////////////////////
	//

	start() {
		const manifest = this.manifest;
		if(!manifest.textures) throw `Manifest does not contain any textures`
		if(!manifest.textures.urls) throw `Manfiest textures do not have any urls`
		this.manifest = manifest;
		this.loader = new PIXI.Loader();

		const assets = [];
		for(var name in manifest.textures.urls) {
			assets.push({
				name,
				url: manifest.textures.urls[name]
			});
		}

		this.loader.add(assets);
		this.loader.onProgress.add(this._handleProgress.bind(this));
		this.loader.load(this._handleComplete.bind(this));
	}

	//
	// --------------------------------------------------------------------------
	//

	getTexture(id) {
		if(!this.resources) return null;
	
		const res = this.resources[id]
		if(!res) throw `Unable to find resource for id ${id}`
		if(!res.texture) throw `No texture loaded for ${id}`
		return res.texture;
	}

	//
	// internal methods /////////////////////////////////////////////////////////
	//
	
	_handleComplete(loader, resources) {
		this.resources = resources;

		const data = this.manifest.load(this);
		for(var key in data) {
			this[key] = data[key];
		}

		this.onLoaded.fire();
	}
	
	//
	// ----------------------------------------------------------------------------
	//
	
	_handleProgress() {
		this.onProgress.fire( this.loader.progress );
	}

} // end class

//
// ############################################################################
//

export default AssetManager

