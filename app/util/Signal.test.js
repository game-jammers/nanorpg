//
// (c) GameJammers 2022
// http://www.jamming.games
//

import Signal from './Signal.js'

//
// ############################################################################
//

test('Default signal name is UNKNOWN', ()=>{
	const signal = new Signal();
	expect(signal).toBeDefined();
	expect(signal.optname).toBe('UNKNOWN');
})

//
// ----------------------------------------------------------------------------
//

test('Signal can be constructed with a name', ()=>{
	const signal = new Signal('Named');
	expect(signal).toBeDefined();
	expect(signal.optname).toBe('Named');
}) 

//
// ----------------------------------------------------------------------------
//

test('Signal can subscribe', ()=>{
	const signal = new Signal('sub');
	expect(signal.listeners.length).toBe(0);
	signal.subscribe(()=>{});
	expect(signal.listeners.length).toBe(1);
})

//
// ----------------------------------------------------------------------------
//

test('Calling subscribe with a falsey value will not add it to the listeners list', ()=>{
	const signal = new Signal('falsey');
	signal.subscribe();
	expect(signal.listeners.length).toBe(0);
})

//
// ----------------------------------------------------------------------------
//

test('You can unsubscribe from signal', ()=>{
	const signal = new Signal('truthy');
	const callback = ()=>{
	}

	expect(signal.listeners.length).toBe(0);
	signal.subscribe(callback);
	expect(signal.listeners.length).toBe(1);
	signal.unsubscribe(callback);
	expect(signal.listeners.length).toBe(0);
})

//
// ----------------------------------------------------------------------------
//

test('fire will call the callbacks', ()=>{
	const signal = new Signal();
	let cbcalled = false;
	const cb = ()=>{ cbcalled = true; }

	signal.subscribe(cb);

	expect(cbcalled).toBe(false);
	signal.fire();
	expect(cbcalled).toBe(true);
})

//
// ----------------------------------------------------------------------------
//

test('unsubscribe removes the correct callback', ()=>{
	const signal = new Signal();

	let cb1called = false; let cb2called = false;
	const cb1 = ()=>{ cb1called = true}
	const cb2 = ()=>{ cb2called = true}

	expect(signal.listeners.length).toBe(0);
	signal.subscribe(cb1);
	expect(signal.listeners.length).toBe(1);
	signal.subscribe(cb2);
	expect(signal.listeners.length).toBe(2);
	signal.unsubscribe(cb1);
	expect(signal.listeners.length).toBe(1);

	expect(cb1called).toBe(false);
	expect(cb2called).toBe(false);

	signal.fire();

	expect(cb1called).toBe(false);
	expect(cb2called).toBe(true);
})
