//
// (c) GameJammers 2022
// http://www.jamming.games
//

//
// ############################################################################
//


const makeEnum = function(obj){
	const result = {};
	for(var key in obj) {
		result[key] = key;
	}
	return result;
}

//
// ############################################################################
//

export default makeEnum;

