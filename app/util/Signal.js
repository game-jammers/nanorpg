//
// (c) GameJammers 2022
// http://www.jamming.games
//

class Signal {
	constructor(optname) {
		this.optname = optname || 'UNKNOWN';
		this.listeners = [];
	}
	
	//
	// public methods ///////////////////////////////////////////////////////////
	//
	
	subscribe(callback) {
		if(callback) {
			this.listeners.push(callback);
		}
	}
	
	//
	// --------------------------------------------------------------------------
	//
	
	unsubscribe(callback) {
		this.listeners = this.listeners.filter((item)=>{
			return item != callback;
		});
	}
	
	//
	// --------------------------------------------------------------------------
	//
	
	fire(params) {
		for(const key in this.listeners) {
			this.listeners[key](params);
		}
	}

} // end class

//
// ############################################################################
//

export default Signal;

