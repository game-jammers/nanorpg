//
// (c) GameJammers 2022
// http://www.jamming.games
//

import * as PIXI from 'pixi.js'

import Point from '@app/math/Point.js'
import Rect from '@app/math/Rect.js'

//
// ############################################################################
//

const TileSize = new Point(16,16);
const TileRect = (x,y,offx,offy)=>new Rect((x+offx)*TileSize.x, (y+offy)*TileSize.y, TileSize.x, TileSize.y);

const Layouts = {
	Tile1x1: (offx,offy)=>TileRect(0,0,offx,offy),
	TileX: (offx,offy,dims)=>new Rect(offx*TileSize.x, offy*TileSize.y, dims.x*TileSize.x, dims.y*TileSize.y),

	Tile3x3: (offx,offy)=>{ return {
		NW: TileRect(0,0,offx,offy), N: TileRect(1,0,offx,offy), NE: TileRect(2,0,offx,offy),
		W:  TileRect(0,1,offx,offy), C: TileRect(1,1,offx,offy), E:  TileRect(2,1,offx,offy),
		SW: TileRect(0,2,offx,offy), S: TileRect(1,2,offx,offy), SE: TileRect(2,2,offx,offy),
	}},

	Border: (offx,offy)=>{ return {
		NW: TileRect(0,0,offx,offy), N: TileRect(1,0,offx,offy), NE: TileRect(2,0,offx,offy),
		W:  TileRect(0,1,offx,offy),												   E: TileRect(2,1,offx,offy),
		SW: TileRect(0,2,offx,offy), S: TileRect(1,2,offx,offy), SE: TileRect(2,2,offx,offy),
	}},

	Tile3x3Ext: (offx,offy)=>{ return {
		NW: TileRect(0,0,offx,offy), N: TileRect(1,0,offx,offy), NE: TileRect(2,0,offx,offy), Top: TileRect(3,0,offx,offy), Single: TileRect(5,0,offx,offy),
		W:  TileRect(0,1,offx,offy), C: TileRect(1,1,offx,offy), E:  TileRect(2,1,offx,offy), Col: TileRect(3,1,offx,offy), Left: TileRect(4,1,offx,offy), Row: TileRect(5,1,offx,offy), Right: TileRect(6,1,offx,offy),
		SW: TileRect(0,2,offx,offy), S: TileRect(1,2,offx,offy), SE: TileRect(2,2,offx,offy), Bot: TileRect(3,2,offx,offy)
	}},

	Wall: (offx,offy)=>{ return {
		NW: TileRect(0,0,offx,offy), H: TileRect(1,0,offx,offy), NE: TileRect(2,0,offx,offy), Fill: TileRect(3,0,offx,offy), TTop: TileRect(4,0,offx,offy),
		V:  TileRect(0,1,offx,offy), C: TileRect(1,1,offx,offy), TLeft: TileRect(3,0,offx,offy), Cross: TileRect(4,0,offx,offy), TRight: TileRect(5,0,offx,offy),
		SW: TileRect(0,2,offx,offy), SE: TileRect(2,2,offx,offy), TBot: TileRect(4,2,offx,offy),
	}},

	Pit: (offx,offy)=>{ return {
		NW: TileRect(0,0,offx,offy), N: TileRect(1,0,offx,offy), NE: TileRect(2,0,offx,offy), Top: TileRect(4,0,offx,offy), SE: TileRect(5,0,offx,offy), Ledge: TileRect(6,0,offx,offy), SW: TileRect(7,0,offx,offy),
		W:  TileRect(0,1,offx,offy), C: TileRect(1,1,offx,offy), E:  TileRect(2,1,offx,offy), Col: TileRect(4,1,offx,offy),
	}},

	loadLayout: (layouts, assets)=>{
		const result = {}
		for(const id in layouts) {
			const layout = layouts[id];
			const texture = assets.getTexture(layout.textureId);

			// single tiles are single textures
			if(layout.tile) {
				const tex = new PIXI.Texture(texture, layout.tile);
				tex.createSprite = ()=>new PIXI.Sprite(tex);
				result[id] = tex;
			}

			// multiple tiles create a tile pack
			else if(layout.tiles) {
				const pack = {}
				for( const tilename in layout.tiles) {
					const rect = layout.tiles[tilename];
					const tex = new PIXI.Texture(texture, rect);
					pack[tilename] = tex;
				}
				pack.createSprite = (k)=>new PIXI.Sprite(pack[k]);
				result[id] = pack;
			}

			// frames make animations
			else if(layout.frames) {
				const pack = []
				for(const idx in layout.frames) {
					const frame = new PIXI.Texture(texture, layout.frames[idx]);
					pack.push(frame);
				}
				pack.createSprite = ()=>new PIXI.AnimatedSprite(pack);
				result[id] = pack;
			}
		}

		return result;
	},
} 

//
// ############################################################################
//

export default Layouts;
