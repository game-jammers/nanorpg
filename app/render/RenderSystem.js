//
// (c) GameJammers 2022
// http://www.jamming.games
//

import * as PIXI from 'pixi.js'
import Point from '@app/math/Point.js'
import Rect from '@app/math/Rect.js'
import Manifest from '@app/assets'

//
// ############################################################################
//

const defaultConfig = {
	width: 720,
	height: 405,
};


//
// ############################################################################
//

class RenderSystem {

	//
	// constructor //////////////////////////////////////////////////////////////
	//
	
	constructor(app) {
		if(!app) throw `RenderSystem created without an app`;
		this.app = app;
	}

	//
	// public methods ///////////////////////////////////////////////////////////
	//

	start(config) {
		config = config || {};
		this.config = { ...defaultConfig, ...config };

		// note: we set our pixel resolution but this is independent of our
		// screen resolution.  the game will act as if they have this config width
		// and height in pixels, but the control will always fill as much space
		// as it can while maintaining the same aspect ratio.
		this.pixi = new PIXI.Application({
			width: this.config.width,
			height: this.config.height,
			antialias: false,
			resolution: 1
		});

		this.pixi.renderer.backgroundColor = 0x202020;
		this.pixi.renderer.autoDensity = true;

		this.rootElement = document.getElementById('root');
		this.rootElement.appendChild(this.pixi.renderer.view);

		const size = this._scalePixi();
		window.onresize = this._handleWindowResized.bind(this);

		// create layers
		this.layers = {
			BACKGROUND: new PIXI.Container(),
			FOREGROUND: new PIXI.Container(),
			OVERLAY: new PIXI.Container(),
			INTERFACE: new PIXI.Container(),
		}

		for(const layerName in this.layers) {
			this.pixi.stage.addChild(this.layers[layerName]);
		}
	}

	//
	// --------------------------------------------------------------------------
	//

	update(dt) {
	}

	//
	// internal methods /////////////////////////////////////////////////////////
	//

	_scalePixi() {
		const size = this._scaleToFit({
			parent: this.rootElement,
			child: this.pixi.renderer.view
		});

		this.pixi.renderer.resize(size.width, size.height);
		this.pixi.stage.scale.x = size.scale.x;
		this.pixi.stage.scale.y = size.scale.y;

		return size;
	}

	//
	// --------------------------------------------------------------------------
	//

	_scaleToFit({parent, child}) {
		const size = { 
			width: child.offsetWidth,
			height: child.offsetHeight,
			scale: 1,
		}

		child.style.position = 'absolute';
		// if our width is wider than our height, stretch to fill our height and put
		// margins at our sides
		if(parent.clientWidth > parent.clientHeight) {
			const ratio = child.offsetWidth / child.offsetHeight;
			size.height = parent.clientHeight;
			size.width = parent.clientHeight * ratio;

			const margin = (parent.clientWidth - size.width) / 2;
			child.style.left = `${margin}px`
			child.style.right = `${margin}px`
			child.style.top = '0px';
			child.style.bottom = '0px';
		}

		// if our height is larger than our width, or if the aspect ratio doesn't give us
		// enough room to stretch vertically, stretch horizontally and put the margin
		// above and below
		if(parent.clientHeight > parent.clientWidth || size.width > parent.clientWidth) {
			const ratio = child.offsetHeight / child.offsetWidth;
			size.width = parent.clientWidth;
			size.height = parent.clientWidth * ratio;

			const margin = (parent.clientHeight - size.height) / 2;
			child.style.left = '0px';
			child.style.right = '0px';
			child.style.top = `${margin}px`;
			child.style.bottom = `${margin}px`;
		}

		size.scale = {
			x: size.width / this.config.width,
			y: size.height / this.config.height,
		};

		return size;
	}

	//
	// --------------------------------------------------------------------------
	//

	_handleWindowResized() {
		this._scalePixi();
	}

} // end class

//
// ############################################################################
//

export default RenderSystem;
