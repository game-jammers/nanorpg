//
// (c) GameJammers 2022
// http://www.jamming.games
//

import textures from './textures'

//
// ############################################################################
//

const Manifest = {
	textures
}

//
// ----------------------------------------------------------------------------
//

Manifest.load = function(assets) {
	return {
		textures: Manifest.textures.load(assets),
	}
}

//
// ############################################################################
//

export default Manifest;

