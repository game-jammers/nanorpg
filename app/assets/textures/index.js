//
// (c) GameJammers 2022
// http://www.jamming.games
//

import characters from './characters'
import environments from './environments'
import overworld from './overworld'

//
// ############################################################################
//

const all = {
	characters,
	environments,
	overworld
}

//
// ----------------------------------------------------------------------------
//

let urls = {};
let ids = {};

for(var key in all) {
	urls = { ...urls, ...all[key].urls};
	ids =  { ...ids,  ...all[key].ids };
}

//
// ############################################################################
//

const Textures = {
	ids,
	urls,
}

//
// ----------------------------------------------------------------------------
//

Textures.load = (assets)=>{
	const result = {}
	for(var key in all) {
		if(all[key].load) {
			result[key] = all[key].load(assets);
		}
	}
	return result;
}

//
// ############################################################################
//

export default Textures;
