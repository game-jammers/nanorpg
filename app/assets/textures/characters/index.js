//
// (c) GameJammers 2022
// http://www.jamming.games
//

import TEX_AQUATIC from './Aquatic.png'
import TEX_AVIAN from './Avian.png'
import TEX_CAT from './Cat.png'
import TEX_DEMON from './Demon.png'
import TEX_DOG from './Dog.png'
import TEX_ELEMENTAL from './Elemental.png'
import TEX_HUMANOID from './Humanoid.png'
import TEX_MISC from './Misc.png'
import TEX_PEST from './Pest.png'
import TEX_PLANT from './Plant.png'
import TEX_PLAYER from './Player.png'
import TEX_QUADRAPED from './Quadraped.png'
import TEX_REPTILE from './Reptile.png'
import TEX_RODENT from './Rodent.png'
import TEX_SLIME from './Slime.png'
import TEX_UNDEAD from './Undead.png'

import Layout from '@app/render/Layout.js'
import makeEnum from '@app/util/makeEnum.js'

//
// ############################################################################
//

const urls = {
	TEX_AQUATIC,
	TEX_AVIAN,
	TEX_CAT,
	TEX_DEMON,
	TEX_DOG,
	TEX_ELEMENTAL,
	TEX_HUMANOID,
	TEX_MISC,
	TEX_PEST,
	TEX_PLANT,
	TEX_PLAYER,
	TEX_QUADRAPED,
	TEX_REPTILE,
	TEX_RODENT,
	TEX_SLIME,
	TEX_UNDEAD
}

//
// ----------------------------------------------------------------------------
//

const textureIds = makeEnum(urls);

//
// ############################################################################
//

const Characters = {
	textureIds,
	urls,
}

//
// ----------------------------------------------------------------------------
//

const tile = (id,t)=>{ return { textureId: id, tile: t } };
const tiles = (id,t)=>{ return { textureId: id, tiles: t } };
const anim = (id,f)=>{ return { textureId: id, frames: f } };

const layouts = {
	// aquatic
	BRUTE:					anim(textureIds.TEX_HUMANOID, [ Layout.Tile1x1(0,0), Layout.Tile1x1(8,0) ]),
	SHIELD_BRUTE:		anim(textureIds.TEX_HUMANOID, [ Layout.Tile1x1(1,0), Layout.Tile1x1(9,0) ]),
	HIGHWAYMAN:			anim(textureIds.TEX_HUMANOID, [ Layout.Tile1x1(2,0), Layout.Tile1x1(10,0) ]),
	PYROMANCER:			anim(textureIds.TEX_HUMANOID, [ Layout.Tile1x1(3,0), Layout.Tile1x1(11,0) ]),
	VIKING:					anim(textureIds.TEX_HUMANOID, [ Layout.Tile1x1(4,0), Layout.Tile1x1(12,0) ]),
	WARMAGE:				anim(textureIds.TEX_HUMANOID, [ Layout.Tile1x1(5,0), Layout.Tile1x1(13,0) ]),
	MAGMA_GOLEM:		anim(textureIds.TEX_HUMANOID, [ Layout.Tile1x1(6,0), Layout.Tile1x1(14,0) ]),
	GLADIATOR:			anim(textureIds.TEX_HUMANOID, [ Layout.Tile1x1(7,0), Layout.Tile1x1(15,0) ]),
}

//
// ----------------------------------------------------------------------------
//

Characters.load = function(assets) {
	const result = Layout.loadLayout(layouts, assets);
	result.ids = makeEnum(layouts);
	return result;
}

//
// ############################################################################
//

export default Characters;
