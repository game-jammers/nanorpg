//
// (c) GameJammers 2022
// http://www.jamming.games
//

import TEX_ENVIRONMENTS from './Environments.png'

import Layout from '@app/render/Layout.js'
import makeEnum from '@app/util/makeEnum.js'

//
// ############################################################################
//

const urls = {
	TEX_ENVIRONMENTS
}

//
// ----------------------------------------------------------------------------
//

const textureIds = makeEnum(urls);

//
// ############################################################################
//

const Environments = {
	textureIds,
	urls
}

//
// ----------------------------------------------------------------------------
//

const tile = (t)=>{ return { textureId: textureIds.TEX_ENVIRONMENTS, tile: t } };
const tiles = (t)=>{ return { textureId: textureIds.TEX_ENVIRONMENTS, tiles: t } };
const anim = (f)=>{ return { textureId: textureIds.TEX_ENVIRONMENTS, frames: f } };

const layouts = {
	//
	// floors
	//
	FLR_BRIGHTBRICK:		tiles( Layout.Tile3x3Ext(0,3) ),
	FLR_BRICK:					tiles( Layout.Tile3x3Ext(0,6) ),
	FLR_DIMBRICK:				tiles( Layout.Tile3x3Ext(0,9) ),
	FLR_DARKBRICK:			tiles( Layout.Tile3x3Ext(0,12) ),

	FLR_BRIGHTGRASS:		tiles( Layout.Tile3x3Ext(7,3) ),
	FLR_GRASS:					tiles( Layout.Tile3x3Ext(7,6) ),
	FLR_DIMGRASS:				tiles( Layout.Tile3x3Ext(7,9) ),
	FLR_DARKGRASS:			tiles( Layout.Tile3x3Ext(7,12) ),

	FLR_BRIGHTSTONE:		tiles( Layout.Tile3x3Ext(14,3) ),
	FLR_STONE:					tiles( Layout.Tile3x3Ext(14,6) ),
	FLR_DIMSTONE:				tiles( Layout.Tile3x3Ext(14,9) ),
	FLR_DARKSTONE:			tiles( Layout.Tile3x3Ext(14,12) ),

	FLR_BRIGHTSAND:			tiles( Layout.Tile3x3Ext(0,15) ),
	FLR_SAND:						tiles( Layout.Tile3x3Ext(0,18) ),
	FLR_DIMSAND:				tiles( Layout.Tile3x3Ext(0,21) ),
	FLR_DARKSAND:				tiles( Layout.Tile3x3Ext(0,24) ),

	FLR_BRIGHTWOOD:			tiles( Layout.Tile3x3Ext(7,15) ),
	FLR_WOOD:						tiles( Layout.Tile3x3Ext(7,18) ),
	FLR_DIMWOOD:				tiles( Layout.Tile3x3Ext(7,21) ),
	FLR_DARKWOOD:				tiles( Layout.Tile3x3Ext(7,24) ),

	FLR_BRIGHTSNOW:			tiles( Layout.Tile3x3Ext(14,15) ),
	FLR_SNOW:						tiles( Layout.Tile3x3Ext(14,18) ),
	FLR_DIMSNOW:				tiles( Layout.Tile3x3Ext(14,21) ),
	FLR_DARKSNOW:				tiles( Layout.Tile3x3Ext(14,24) ),

	FLR_BRIGHFIELD:			tiles( Layout.Tile3x3Ext(0,27) ),
	FLR_FIELD:					tiles( Layout.Tile3x3Ext(0,30) ),
	FLR_DIMFIELD:				tiles( Layout.Tile3x3Ext(0,33) ),
	FLR_DARKFIELD:			tiles( Layout.Tile3x3Ext(0,36) ),

	FLR_STONE:					tile( Layout.Tile1x1(8,28) ),
	FLR_WATER0:					tile( Layout.Tile1x1(9,28) ),
	FLR_WATER1:					tile( Layout.Tile1x1(10,28) ),
	FLR_ICE:						tile( Layout.Tile1x1(11,28) ),
	FLR_ACID:						tile( Layout.Tile1x1(12,28) ),
	FLR_LAVA:						tile( Layout.Tile1x1(13,28) ),
	FLR_LAVA_ROCK:			tile( Layout.Tile1x1(14,28) ),
	FLR_WATER2:					tile( Layout.Tile1x1(15,28) ),

	FLR_SLAB0:					tile( Layout.Tile1x1(8,30) ),
	FLR_SLAB1:					tile( Layout.Tile1x1(9,30) ),
	FLR_SLAB2:					tile( Layout.Tile1x1(10,30) ),
	FLR_SLAB3:					tile( Layout.Tile1x1(11,30) ),
	FLR_SLAB4:					tile( Layout.Tile1x1(12,30) ),
	FLR_SLAB5:					tile( Layout.Tile1x1(13,30) ),
	FLR_TILE:						tile( Layout.Tile1x1(14,30) ),
	FLR_DIAMOND:				tile( Layout.Tile1x1(15,30) ),
	
	FLR_BRIDGEV:				tile( Layout.Tile1x1(8,31) ),
	FLR_BRIDGEH:				tile( Layout.Tile1x1(9,31) ),
	
	STR_UP_STONE:				anim([ Layout.Tile1x1(8,29), Layout.Tile1x1(10,29) ]),
	STR_DOWN_STONE:			anim([ Layout.Tile1x1(9,29), Layout.Tile1x1(11,29) ]),
	STR_UP_WOOD:				anim([ Layout.Tile1x1(12,29), Layout.Tile1x1(14,29) ]),
	STR_DOWN_WOOD:			anim([ Layout.Tile1x1(13,29), Layout.Tile1x1(15,29) ]),

	//
	// walls
	//
	WALL_BRIGHTBRICK:		tiles( Layout.Wall(22,3) ),
	WALL_BRICK:					tiles( Layout.Wall(22,6) ),
	WALL_DIMBRICK:			tiles( Layout.Wall(22,9) ),
	WALL_DARKBRICK:			tiles( Layout.Wall(22,12) ),

	WALL_BRIGHTLOG:			tiles( Layout.Wall(29,3) ),
	WALL_LOG:						tiles( Layout.Wall(29,6) ),
	WALL_DIMLOG:				tiles( Layout.Wall(29,9) ),
	WALL_DARKLOG:				tiles( Layout.Wall(29,12) ),

	WALL_BRIGHTCAVE:		tiles( Layout.Wall(36,3) ),
	WALL_CAVE:					tiles( Layout.Wall(36,6) ),
	WALL_DIMCAVE:				tiles( Layout.Wall(36,9) ),
	WALL_DARKCAVE:			tiles( Layout.Wall(36,12) ),
	
}

//
// ----------------------------------------------------------------------------
//


Environments.load = function(assets) {
	const result = Layout.loadLayout(layouts, assets);
	result.ids = makeEnum(layouts);
	return result;
}

//
// ############################################################################
//

export default Environments
