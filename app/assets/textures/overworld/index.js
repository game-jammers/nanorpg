//
// (c) GameJammers 2022
// http://www.jamming.games
//

import TEX_OVERWORLD from './overworld.png'

import makeEnum from '@app/util/makeEnum.js'
import Layout from '@app/render/Layout.js'
import * as PIXI from 'pixi.js'
import Point from '@app/math/Point.js'
import Rect from '@app/math/Rect.js'

//
// ############################################################################
//

const urls = {
	TEX_OVERWORLD
}

//
// ----------------------------------------------------------------------------
//

const textureIds = makeEnum(urls);


//
// ############################################################################
//


const Overworld = {
	urls
}

//
// ----------------------------------------------------------------------------
//

const tile = (t)=>{ return { textureId: textureIds.TEX_OVERWORLD, tile: t }}
const tiles = (t)=>{ return { textureId: textureIds.TEX_OVERWORLD, tiles: t }}

const layouts = {
	WOOD0:					tile( Layout.Tile1x1(0,0) ),
	CARPET:					tile( Layout.Tile1x1(1,0) ),
	SAND:						tile( Layout.Tile1x1(2,0) ),
	GRASS0:					tile( Layout.Tile1x1(3,0) ),
	FLOWERS:				tile( Layout.Tile1x1(4,0) ),
	WATER0:					tile( Layout.Tile1x1(5,0) ),
	STONE:					tile( Layout.Tile1x1(6,0) ),
	CLIFF0:					tile( Layout.Tile1x1(7,0) ),
	GRASS1:   			tile( Layout.Tile1x1(8,0) ),
	GRASS2:   			tile( Layout.Tile1x1(9,0) ),
	MTNRANGE0:			tiles( Layout.Tile3x3(10,0) ),
	MTNRANGE1: 			tiles( Layout.Tile3x3(13,0) ),

	WOOD1:					tile( Layout.Tile1x1(0,1) ),
	CARPET_BLOCK:		tile( Layout.Tile1x1(1,1) ),
	TOP_SAND:				tile( Layout.Tile1x1(2,1) ),
	TOP_GRASS:			tile( Layout.Tile1x1(3,1) ),
	TOP_BUSH:				tile( Layout.Tile1x1(4,1) ),
	WATERFALL:			tile( Layout.Tile1x1(5,1) ),
	BLACK:					tile( Layout.Tile1x1(6,1) ),
	CLIFF1:					tile( Layout.Tile1x1(7,1) ),
	DIRT:						tile( Layout.Tile1x1(8,1) ),
	COBBLE:					tile( Layout.Tile1x1(9,1) ),

	TOP_CAUSTICS0:	tile( Layout.Tile1x1(3,2) ),
	TOP_PINE:				tile( Layout.Tile1x1(4,2) ),
	GEMS:						tile( Layout.Tile1x1(5,2) ),
	GOLD:						tile( Layout.Tile1x1(6,2) ),
	SIGN:						tile( Layout.Tile1x1(7,2) ),
	LAVACLIFF:			tile( Layout.Tile1x1(8,2) ),

	TOP_CAUSTICS1:	tile( Layout.Tile1x1(3,3) ),
	TOP_OAK:				tile( Layout.Tile1x1(4,3) ),
	TOP_TWOTREE:		tile( Layout.Tile1x1(5,3) ),
	LAVA0:					tile( Layout.Tile1x1(7,3) ),
	LAVA1:					tile( Layout.Tile1x1(8,3) ),

	GROVE:					tiles( Layout.Tile3x3(0,5) ),

	CASTLE:					tile( Layout.TileX(3,4, new Point(3,3)) ),
	TOWER:					tile( Layout.TileX(6,4, new Point(1,3)) ),

	WATER1:					tile( Layout.Tile1x1(7,4) ),
	WATER2:					tile( Layout.Tile1x1(8,4) ),

	FORT:						tile( Layout.TileX(3,8, new Point(2,1)) ),
	KEEP:						tile( Layout.TileX(5,7, new Point(3,3)) ),	

	GRASS_CAVE:			tile( Layout.Tile1x1(8,5) ),
	LAVA_CAVE:			tile( Layout.Tile1x1(8,6) ),
	SAND_CAVE:			tile( Layout.Tile1x1(8,7) ),

	MTN0:						tile( Layout.Tile1x1(9,5) ),
	MTN1:						tile( Layout.Tile1x1(10,5) ),
}

//
// ----------------------------------------------------------------------------
//

Overworld.load = (assets)=>{
	const result = Layout.loadLayout(layouts, assets);
	result.ids = makeEnum(layouts);
	return result;
}

//
// ############################################################################
//

export default Overworld;
