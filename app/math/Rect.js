//
// (c) GameJammers 2022
// http://www.jamming.games
//

import { Rectangle as Rect } from 'pixi.js'

//
// ############################################################################
//

export default Rect;
