//
// (c) GameJammers 2022
// http://www.jamming.games
//

import srand from 'seeded-rand';

//
// ############################################################################
//

const Random = {

	// https://www.partow.net/programming/hashfunctions/index.html#DEKHashFunction
	dek: (str)=>{
		let hash = 0;
		for(i = 0; i < str.length; ++i) {
			const c = str.charCodeAt(i);
			hash = ((hash << 5) ^ (hash << 27)) ^ c;
		}
		return hash;
	},

	// https://github.com/bryc/code/blob/master/jshash/PRNGs.md
	mulberry32: (a)=>{
			a |= 0; a = a + 0x6D2B79F5 | 0;
      var t = Math.imul(a ^ a >>> 15, 1 | a);
      t = t + Math.imul(t ^ t >>> 7, 61 | t) ^ t;
      return ((t ^ t >>> 14) >>> 0) / 4294967296;
	},

	//
	rng: (seed)=>{
		return new srand(seed);
	},
}

//
// ############################################################################
//

export default Random;
