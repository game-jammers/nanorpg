//
// (c) GameJammers 2022
// http://www.jamming.games
//

import '@app/style/index.css'
import * as PIXI from 'pixi.js'


import AssetManager from '@app/AssetManager.js'
import GameState from '@app/game/GameState.js'
import Manifest from '@app/assets'
import Point from '@app/math/Point'
import RenderSystem from '@app/render/RenderSystem.js'
import TestMode from '@app/game/TestMode.js'

//
// ############################################################################
//

const START_MODE = TestMode;

//
// ############################################################################
//

class App {
	constructor() {
		this.manifest = Manifest;
		this.ticker = new PIXI.Ticker();
		this.ticker.add(this._update.bind(this));
	
		this.assets = new AssetManager(this.manifest);
		this.assets.onProgress.subscribe(this._onLoadProgress.bind(this));
		this.assets.onLoaded.subscribe(this._onLoaded.bind(this));
	
		this.render = new RenderSystem(this);
		this.mode = null;
	}

	//
	// public methods ///////////////////////////////////////////////////////////
	//

	get state() {
		if(!this.mode) {
			return GameState.READY;
		}

		return this.mode.state;
	}

	//
	// ----------------------------------------------------------------------------
	//

	start() {
		this.switchMode(START_MODE);
		this.assets.start();
		this.render.start();

		this._startLoading();
	}

	//
	// --------------------------------------------------------------------------
	//

	switchMode(newMode) {
		this.nextMode = newMode;
	}

	//
	// internal methods /////////////////////////////////////////////////////////
	//

	_update(dt) {
		this.render.update(dt);

		// switch mode
		if(this.nextMode) {
			if(this.mode) {
				this.mode.end();
			}
			this.mode = new this.nextMode(this);
			this.nextMode = null;
			this.mode.start();
		}

		// update mode
		if(this.mode) {
			this.mode.update(dt);
		}
	}

	//
	// ----------------------------------------------------------------------------
	//

	_startLoading() {
		const loadingTextStyle = new PIXI.TextStyle({ fill: '#555555', fontSize: 10, align: 'center' });
		this.loading = {
			root: new PIXI.Container(),
			bgbar: new PIXI.Graphics(),
			fgbar: new PIXI.Graphics(),
			text: new PIXI.Text('0.0%', loadingTextStyle),
			textMetrics: new PIXI.TextMetrics.measureText('Loading... 100%', loadingTextStyle),
		}

		let self = this;
		const center = new Point(this.render.config.width / 2, this.render.config.height / 2);
		this.loading.mount = ()=>self.render.layers.INTERFACE.addChild(this.loading.root);
		this.loading.unmount = ()=>self.render.layers.INTERFACE.removeChild(this.loading.root);

		const halfSize = new Point(300,10);
		this.loading.bgbar.lineStyle(1, 0xffffffff);
		this.loading.bgbar.beginFill(0x00000000);
		this.loading.bgbar.drawRect(center.x-halfSize.x, center.y-halfSize.y, halfSize.x*2, halfSize.y*2);
		this.loading.bgbar.endFill();
		this.loading.bgbar.zOrder = -1;
		this.loading.root.addChild(this.loading.bgbar);

		this.loading.fgbar.lineStyle(1, 0xffffffff);
		this.loading.fgbar.beginFill(0xffffffff);
		this.loading.fgbar.drawRect(center.x-halfSize.x, center.y-halfSize.y, halfSize.x*2, halfSize.y*2);
		this.loading.fgbar.endFill();
		this.loading.fgbar.zOrder = -1;
		this.loading.root.addChild(this.loading.fgbar);

		this.loading.text.x = center.x-this.loading.textMetrics.width/2;
		this.loading.text.y = center.y-this.loading.textMetrics.height/2;
		this.loading.text.width = this.loading.textMetrics.width;
		this.loading.text.height = this.loading.textMetrics.height;
		this.loading.text.zOrder = 1;
		this.loading.root.addChild(this.loading.text);
		
		this.loading.mount();
	}

	_onLoadProgress(prog) {
		this.loading.fgbar.width = this.loading.bgbar.width * prog / 100;
		this.loading.text.text = `Loading ${prog}%`;
		console.log(`LOADING... ${prog}`);
	}

	//
	// ----------------------------------------------------------------------------
	//

	_onLoaded() {
		// start updating the main loop
		this.loading.unmount();
		this.ticker.start();
	}

} // end class

//
// ############################################################################
//

export default App;
