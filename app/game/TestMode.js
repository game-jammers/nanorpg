//
// (c) GameJammers 2022
// http://www.jamming.games
//

import GameMode from './GameMode.js'
import GameState from './GameState.js'
import OverworldGenerator from '@app/world/OverworldGenerator.js'

//
// ############################################################################
//

class TestMode extends GameMode {
	constructor(app) {
		super(app);
		this.generator = new OverworldGenerator('seed');
	}

	//
	// //////////////////////////////////////////////////////////////////////////
	//

	get state() {
		return GameState.TESTING;
	}

	//
	// --------------------------------------------------------------------------
	//
	
	start() {
		super.start();
		this.generator.build(this.app.assets);
	}

	//
	// --------------------------------------------------------------------------
	//
	
	update(dt) {
		super.update(dt);
	}

	//
	// --------------------------------------------------------------------------
	//
	
	end() {
		super.end();
	}

} // end class


//
// ############################################################################
//

export default TestMode;

