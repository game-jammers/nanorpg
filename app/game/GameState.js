//
// (c) GameJammers 2022
// http://www.jamming.games
//

//
// ############################################################################
//

const GameState = {
	READY: 'READY',
	LOADING: 'LOADING',
	TESTING: 'TESTING',
	UNKNOWN: 'UNKNOWN',
}

//
// ############################################################################
//

export default GameState;
