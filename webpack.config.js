//
// (c) GameJammers 2022
// http://www.jamming.games
//

const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const os = require('os')
const path = require('path')

//
// ////////////////////////////////////////////////////////////////////////////
//

const isDev = process.env.NODE_ENV !== 'production';

//
// ############################################################################
//

module.exports = {
	mode: isDev ? 'development' : 'production',
	entry: './index.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js',
		publicPath: '/',
	},

	//
	// --------------------------------------------------------------------------
	//
	
	module: {
		rules: [

			// source map
			{
				test: /\.js$/,
				enforce: 'pre',
				use: ['source-map-loader'],
			},

			// javascript transpilation
			{
				test: /\.js?$/,
				exclude: /node_modules/,
				use: [ 'babel-loader' ]
			},

			// css
			{
				test: /\.css$/,
				use: [ MiniCssExtractPlugin.loader, 'css-loader' ]
			},

			// images
			{
				test: /\.(jpg|jpeg|png|svg)$/,
				use: [ 'file-loader' ]
			}
		]
	},

	//
	// --------------------------------------------------------------------------
	//

	resolve: {
		extensions: [ '.js', '.svelte' ],
		alias: {
			'@app': path.join(__dirname, 'app')
		}
	},

	//
	// --------------------------------------------------------------------------
	//

	plugins: [
		new HtmlWebpackPlugin({
			template: path.resolve(__dirname, 'public', 'index.html')
		}),

		new MiniCssExtractPlugin(),
	],

	//
	// --------------------------------------------------------------------------
	//

	devServer: {
		historyApiFallback: true,
		static: [ 'public' ],
		allowedHosts: 'all',
		port: 9010,
		hot: true,
	},

	//
	// --------------------------------------------------------------------------
	//
	
	ignoreWarnings: [
	],
}
