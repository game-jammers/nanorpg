//
// (c) GameJammers 2022
// http://www.jamming.games
//

import App from '@app/app'

const app = new App();
app.start();


